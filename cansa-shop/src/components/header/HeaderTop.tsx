import './headerTop.css';
import { useState, useEffect } from 'react';
import { Search, ShoppingCartOutlined } from '@material-ui/icons';
import { Link } from "react-router-dom";
import MenuTop from '../menu/MenuTop';
import { checkLogin, logout, getUserInfo } from '../../redux/actions/userActions'
import { CartModel, getCart, State, updateAccess, UserModel, UserStage } from '../../redux';
import { useDispatch, useSelector } from 'react-redux';

export default function HeaderTop() {
    const [textInput, setTextInput] = useState("");
    const { cart }: { cart: CartModel } = useSelector((state: State) => state.cartReducer);
    const userState: UserStage = useSelector((state: State) => state.userReducer);
    const { check, status, userInfor }: { check: boolean, status: string, userInfor: UserModel } = userState;
    const dispatch = useDispatch();
    const [number, setNumber] = useState(0)

    useEffect(() => {
        dispatch(checkLogin());
    }, [status])

    useEffect(() => {
        dispatch(updateAccess());
        dispatch(getCart());
    }, [])

    useEffect(() => {
        if (cart?.cart) {
            setNumber(cart.cart.length);
        } else {
            setNumber(0)
        }
    }, [cart])

    const btnLogout = () => {
        dispatch(logout());
    }

    useEffect(() => {
        dispatch(getUserInfo());
    }, [check])

    //lay text input
    const changeText = (event: any) => {
        setTextInput(event.target.value)
    }

    return (
        <>
            <div className="header__container">
                <div className="header__container-con">
                    <div className="header__container-top">
                        <div className="header__container-welcome">
                            Chào mừng đến với CANSA SHOP
                        </div>

                        {/*class auth-hidden: display:none*/}
                        {
                            !check ?
                                <ul className="header__container-auth">
                                    <li className="header__container-auth-login header__container-auth-separate">
                                        <Link className="menu-item-link-header" to="/Login">Login</Link>
                                    </li>
                                    <li className="header__container-auth-register">
                                        <Link className="menu-item-link-header" to="/Register">Register</Link>
                                    </li>
                                </ul>
                                :
                                <ul className="header__container-auth">
                                    <li className="header__container-auth-login header__container-auth-separate">
                                        <Link className="menu-item-link-header" to="/account">{userInfor.user_name}</Link>
                                    </li>
                                    <li className="header__container-auth-register">
                                        <button className="menu-item-link-header" onClick={() => btnLogout()}>Logout</button>
                                    </li>
                                </ul>
                        }
                        <div className="logout auth-hidden">
                            Logout
                        </div>
                    </div>

                    <div className="header__container-bot">
                        <div className="header__container-bot-left">
                            <Link className="menu-item-link" to="/">CANSA</Link>
                        </div>
                        <div className="header__container-bot-center">

                            <input value={textInput} placeholder="Bạn đang tìm kiếm gì ?" className="input-search" maxLength={255} type="text" onChange={changeText} />

                            <Link className="menu-item-link-header" to={`/products/search/${textInput}`}>
                                <div className="search-top">
                                    <Search />
                                </div>
                            </Link>
                        </div>

                        <div className="header__container-bot-right">

                            <Link className="menu-item-link" to="/cart">
                                <div className="Shopping-Cart">
                                    <ShoppingCartOutlined style={{ fontSize: 35, color: 'white' }} />
                                    <div className="header__num-cart">{number}</div>
                                </div>
                            </Link>

                        </div>
                    </div>


                </div>
            </div>
            <div className="menu-bot">
                <MenuTop />
            </div>
        </>
    )
}
