import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { checkLogin, State, UserStage } from '../redux';

export default function IsLogin() {

    const userState: UserStage = useSelector((state: State) => state.userReducer);
    const { check }: { check: boolean } = userState;
    const history = useHistory();
    const dispatch = useDispatch();
    
    useEffect(() => {
        dispatch(checkLogin());
    }, [])

    useEffect(() => {
        !check && history.push("/");
    }, [check])
    return (
        <>

        </>
    )
}
