import { ArrowForwardIos } from '@material-ui/icons';
import { useState, useRef } from 'react';
import Slider from 'react-slick';
import './gallery.css'
export default function Gallery(props:any) {
  const {avatar,images} =props;
  const gallerySlider = useRef<any>();
  const [indexSelected, setIndexSelected] = useState(0);
  const next = () => {
    gallerySlider.current.slickNext();
  };
  let dataSlider = [avatar,...images];
    return (
        <div>
            <div className="gallery-img-container">
                <div className="gallery-img-product">
                    <img src={dataSlider[indexSelected]} alt="" />
                </div>

                <div className="gallery-slider">
                    <Slider ref={gallerySlider} className="gallery-list-slider" {...settings}>
                        {
                            dataSlider.map((item, index) => <div key={index} onClick={()=>setIndexSelected(index)} className= {index === indexSelected ?"img-gallery-selected":"img-gallery"} ><img src={item} alt={item}/></div>)
                        }
                    </Slider>
                    <button className="btn-gallery btn-next-gallery" onClick={next}><ArrowForwardIos style={{color:"black",fontSize:22,fontWeight:'bold'}} /></button>
                </div>
            </div>
        </div>
    )
}
const settings = {
    focusOnSelect: true,
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    speed: 500,
    dots: false,
    arrows:false,
    responsive: [
      {
        breakpoint: 1023,
        settings: {
          focusOnSelect: true,
          infinite: true,
          slidesToShow: 3,
          slidesToScroll: 1,
          dots: false,
          arrows:false,
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          arrows:false,
          initialSlide: 0
        }
      },
      
    ]
};