import './slideitem.css';

export default function SlideItem(props:any) {
    const{ slider } = props;
    return (
        <div className="slideitem-container">
            <img src={slider.slider_image} alt={slider.slider_image} />
            <div className="slideitem-title">{slider.slider_title}</div>
        </div>
    )
}
