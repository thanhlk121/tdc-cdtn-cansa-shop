import { StarRate } from '@material-ui/icons';
import { useState } from 'react';
import './rating.css'

export default function Rating(props: any) {
    const [text, setText] = useState<any>()
    const { onTap } = props;
    const [defautRating, setDefautRating] = useState(5);
    const maxRating = [1, 2, 3, 4, 5];

    return (
        <div className="rating-container">
            <div className="rating-comment">
                <div className="rating-star">
                    {
                        maxRating.map((item, index) =>
                            item <= defautRating ?
                                <StarRate key={index} onClick={() => setDefautRating(item)} style={{ fontSize: 30, color: '#ffd700' }} /> : <StarRate onClick={() => setDefautRating(item)} style={{ fontSize: 30, color: '#808080' }} />
                        )
                    }
                </div>
                <div className="rating-btn">
                    <button onClick={() => {
                        onTap(text, defautRating)
                        setText('');
                        setDefautRating(5);
                    }
                    } className="rating-btn-send">Send</button>
                </div>
            </div>
            <textarea onChange={(e: any) => setText(e.target.value)} value={text} maxLength={255} className="txt-comment" placeholder="Your comment . . ." />

        </div>
    )
}
