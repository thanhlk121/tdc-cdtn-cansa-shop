import React, { useEffect, useState } from 'react'
import Footer from '../../components/footer/Footer'
import HeaderTop from '../../components/header/HeaderTop'
import StorefrontIcon from '@material-ui/icons/Storefront';
import './shopdetail.css'
import { getProductsShop, ProductModel, ShopModel, State } from '../../redux';
import Loading from '../../components/loading/Loading';
import Product from '../../components/Product/Product';
import { useDispatch, useSelector } from 'react-redux';
import { getShopInfo } from '../../redux/actions/shopActions';
import { useParams } from 'react-router';
import { Link } from 'react-router-dom';

export default function ShopDetail() {
    let { shop_id } = useParams<any>();
    const [page, setPage] = useState<number>(1);
    const productState = useSelector((state: State) => state.productReducer);
    const shopState = useSelector((state: State) => state.shopReducer);
    const dispatch = useDispatch();
    const { info }: { info: ShopModel } = shopState;
    const { productShop }: { productShop: ProductModel[] } = productState;
    const [_productShop, setProductShop] = useState<any>();

    useEffect(() => {
        dispatch(getShopInfo(shop_id));
        dispatch(getProductsShop(shop_id));
    }, [])

    useEffect(() => {
        if (info && productShop) {
            setProductShop(productShop);
        }
    }, [shopState, productState])

    useEffect(() => {
        dispatch(getProductsShop(shop_id, page));
    }, [page])

    return (
        <>
            <HeaderTop></HeaderTop>

            <div className="shopdetail_container">
                <div className="container_con_shopdetail">
                    <div className="title_name_shop">
                        <div className="avatar_shop">
                            <img src={info.shop_avatar} />
                            <div className="shopdetail_infor">
                                <div className="shop_name"><h3>{info.shop_name}</h3></div>
                                <div className="shop_email"><StorefrontIcon id="icon_shopinfor"></StorefrontIcon> {info.shop_description}</div>
                                <button className="productDetail-btn-check"><Link className="productdetail-link-shop" to={`/chat/${shop_id}`}>Chat</Link></button>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div className="home-container">
                <div className="home-container-product">
                    {
                        _productShop ?
                            _productShop.map((item: ProductModel, index: number) => <Product product={item} key={index} />)
                            :
                            <Loading />
                    }
                </div>
            </div>


            <Footer></Footer>
        </>
    )
}

