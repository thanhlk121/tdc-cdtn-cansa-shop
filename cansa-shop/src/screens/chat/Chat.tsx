import { useState, useEffect } from 'react';
import './chat.css'
import { State, UserModel, UserStage } from '../../redux';
import { useSelector } from 'react-redux';
import HeaderTop from '../../components/header/HeaderTop'
import io from "socket.io-client";
import SendIcon from '@material-ui/icons/Send';
import axios from 'axios';
import { chatSever } from '../../consts/Selector'
import { useParams } from 'react-router';


export default function Chat() {
	let { id } = useParams<any>();
	const [message, setMessage] = useState('')
	const [dataAll, setDataAll]: any = useState([])
	const [hisID, setHisID]: any = useState('')
	const [typingStr, setTypingStr] = useState('')
	const [nameChat, setNameChat] = useState('')
	const [imgChat, setImgChat] = useState('')
	const [myID, setMyID]: any = useState('')
	const [updateList, setUpdateList]: any = useState(true)
	const [statusChat, setChatStatus]: any = useState(false)
	const userState: UserStage = useSelector((state: State) => state.userReducer);
	const { userInfor }: { userInfor: UserModel } = userState;
	const [chatString, setChatString] = useState<any[]>([])
	const socket = io(chatSever);
	var az: any = document.querySelector('.msg_card_body');

	useEffect(() => {

		if (hisID !== '' && myID !== '') {
			(async () => {
				await axios.get<any>(`${chatSever}/api/chat/getChatHistory/${myID}/${hisID}/1/100`)
					.then(res => {
						axios.get<any>(`${chatSever}/api/chat/getChatHistory/${hisID}/${myID}/1/100`)
							.then(res2 => {
								let data_chat_all: any = [];
								res2.data.data.forEach((element: any) => {
									let dataMesss = {
										"_id": '' + Math.random().toString(36).substr(2, 16),
										"createdAt": element.CreateDate,
										"text": element.message,
									}
									data_chat_all.push(dataMesss);
								});
								res.data.data.forEach((element: any) => {
									let dataMesss = {
										"_id": '' + Math.random().toString(36).substr(2, 16),
										"createdAt": element.CreateDate,
										"text": element.message,
										"user": {},
									}
									data_chat_all.push(dataMesss);
								});
								let temp = data_chat_all.sort((a: any, b: any) => new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime())
								temp.reverse();
								let tempData: any = [];
								temp.map((item: any) => {
									if (item.user !== undefined) {
										const showMess = (<div className="d-flex justify-content-end mb-4">
											<div className="msg_cotainer_send">
												{item.text}
												<span className="msg_time_send">8:55 AM, Today</span>
											</div>
										</div>)
										tempData.push(showMess)
										//setChatString([...chatString, showMess])
									} else {
										const showMess = (<div className="d-flex justify-content-start mb-4">
											<div className="msg_cotainer">
												{item.text}
												<span className="msg_time_send">8:55 AM, Today</span>
											</div>
										</div>)
										tempData.push(showMess)
									}
								})
								socket.emit('watched', myID, hisID)
								setChatString(tempData)
							})
					})
			})()
		} else if (id !== undefined) {
			(async () => {
				setHisID('shop_' + id)
				let a: any = await axios.get(`https://103.207.38.200/api/shop/info/${id}/1/e4611a028c71342a5b083d2cbf59c494`)
				setNameChat(a.data.data.shop_name)
				setImgChat(a.data.data.shop_avatar)

			})()
		}
		
		socket.on("connect", () => {
			socket.emit("join", "Hello server from client");
		});


		socket.on("thread", function (data: any) {
			if (data.user_to == myID && data.user_id == hisID) {
				const showMess = (<div className="d-flex justify-content-start mb-4">
					<div className="msg_cotainer">
						{data.message}
						<span className="msg_time_send">8:55 AM, Today</span>
					</div>
				</div>)
				setChatString([...chatString, showMess])
			}
		});
		if (statusChat === true) {
			setChatStatus(false)
		} 
	}, [hisID, myID, statusChat])
	useEffect(() => {
		if (userInfor.user_id !== undefined) {
			setMyID('user_' + userInfor.user_id)
			let temp: any = []
			socket.on("thread", function (data) {
				if (data.user_to === myID) {
					setDataAll([])
					if (statusChat === false) {
						setChatStatus(true)
					} 
					socket.emit('roomList', myID)
				}
			});
			socket.emit('roomList', myID)
			socket.on("roomList", async function (data) {
				let tempData: any = [];
				if (data.length !== 1) {
					await Promise.all(data.map(async (element: any) => {
						socket.emit('onlineStatus', element[0].user_to)
						let id = '_' + Math.random().toString(36).substr(2, 16)
						let id_shop = (myID != element[0].user_to) ? element[0].user_to : element[0].user_from
						let a: any = await axios.get(`https://103.207.38.200/api/shop/info/${id_shop.split('shop_')[1]}/1/e4611a028c71342a5b083d2cbf59c494`)
						tempData.push({
							id: id,
							title: a.data.data.shop_name,
							img: a.data.data.shop_avatar,
							id_user: (myID != element[0].user_to) ? element[0].user_to : element[0].user_from,
							newStatusMess: (element[0].isWatched === 1) ? false : true,
							statusOnline: false,
							CreateDate: element[0].CreateDate,
							text: element[0].message,
						})
					}));
				} else {
					socket.emit('onlineStatus', data[0].user_to)
					let id_shop = (myID != data[0].user_to) ? data[0].user_to : data[0].user_from
					let a: any = await axios.get(`https://103.207.38.200/api/shop/info/${id_shop.split('shop_')[1]}/1/e4611a028c71342a5b083d2cbf59c494`)
					tempData.push({
						id: '_' + Math.random().toString(36).substr(2, 16),
						title: a.data.data.shop_name,
						img: a.data.data.shop_avatar,
						id_user: (myID != data[0].user_to) ? data[0].user_to : data[0].user_from,
						newStatusMess: (data[0].isWatched == 1) ? false : true,
						statusOnline: false,
						CreateDate: data[0].CreateDate,
						text: data[0].message,
					})
				}
				let temp = tempData.sort((a: any, b: any) => new Date(b.CreateDate).getTime() - new Date(a.CreateDate).getTime())
				setDataAll(temp)
				setUpdateList(false)
			});

		}
	}, [updateList, userInfor, myID])
	function timeoutFunction() {
		var typo = {
			user_to: hisID,
			user_id: myID,
			status: false
		}
		socket.emit("is_typing", typo);
	}

	const chatTyping = (e: any) => {
		var timeout;
		if (e.which !== 13) {
			var typo = {
				user_to: hisID,
				user_id: myID,
				status: true
			}
			socket.emit("is_typing", typo);
			clearTimeout(timeout);
			timeout = setTimeout(timeoutFunction, 2000);
		} else {
			clearTimeout(timeout);
			timeoutFunction();
		}
	}

	const chat = () => {
		if (message !== '') {
			let DateNow = new Date;
			var msgDetails = {
				user_id: myID,
				username: 'Hoàng Anh',
				user_to: hisID,
				message: message,
				image: '',
				base64: ''
			};

			socket.emit("messages", msgDetails);
			const showMess = (<div className="d-flex justify-content-end mb-4">
				<div className="msg_cotainer_send">
					{message}
					<div><span className="msg_time_send">8:55 AM, Today</span></div>
				</div>
			</div>)
			setChatString([...chatString, showMess])
			setMessage('');
			if(hisID !== ''){
				az.scrollTop = az.scrollHeight
			}
			setUpdateList(true)

		}
	}
	socket.on("typing", function (data) {
		if (data.user_to === myID && data.user_id === hisID) {
			if (data.status === true) {
				setTypingStr('Đang Nhập....')
			} else {
				setTypingStr('')
			}
		}
	});
	useEffect(()=>{
		if(hisID !== ''){
			console.log(123)
			az.scrollTop = az.scrollHeight
		}
	},[chatString])
	const changeAvtive = (id: any) => {
		const a: any = document.querySelector('.contacts')?.childNodes;
		a.forEach((element: any) => {
			if (element.id === id) {
				element.classList.add("active");
			} else {
				element.classList.remove("active");
			}
		});
	}

	return (
		<>
			<HeaderTop></HeaderTop>
			<div className="container-fluid h-100 margin-chat">
				<div className="row justify-content-center h-100">
					<div className="col-md-4 col-xl-3 chat"><div className="card mb-sm-3 mb-md-0 contacts_card">
						<div className="card-header">
							<div className="input-group">
								<input type="text" placeholder="Search..." name="" className="form-control search"></input>
								<div className="input-group-prepend">
									<span className="input-group-text search_btn"><i className="fas fa-search"></i></span>
								</div>
							</div>
						</div>
						<div className="card-body contacts_body">
							<ul className="contacts">
								{
									dataAll.map((item: any, i: any) => (
										<li key={i} id={item.id_user} onClick={(e) => { setHisID(item.id_user); changeAvtive(item.id_user); setNameChat(item.title); setImgChat(item.img); }} style={{ cursor: "pointer", }}>
											<div className="d-flex bd-highlight" >
												<div className="img_cont">
													<img src={item.img} className="rounded-circle user_img"></img>
													<span className="online_icon"></span>
												</div>
												<div className="user_info">
													<span>{item.title}</span>
													{item.newStatusMess ? (<p><b>{item.text}</b></p>) : (<p>{item.text}</p>)}
												</div>
											</div>
										</li>
									))
								}


							</ul>
						</div>
						<div className="card-footer"></div>
					</div></div>
					<div className="col-md-8 col-xl-6 chat">
						{(hisID !== '') ?
							<div className="card">
								<div className="card-header msg_head">
									<div className="d-flex bd-highlight">
										<div className="img_cont">
											<img src={imgChat} className="rounded-circle user_img"></img>
											<span className="online_icon"></span>
										</div>
										<div className="user_info">
											<span>{nameChat}</span>
										</div>
										<div className="video_cam">
											<span><i className="fas fa-video"></i></span>
											<span><i className="fas fa-phone"></i></span>
										</div>
									</div>
									<span id="action_menu_btn"><i className="fas fa-ellipsis-v"></i></span>
									<div className="action_menu">
										<ul>
											<li><i className="fas fa-user-circle"></i> View profile</li>
											<li><i className="fas fa-users"></i> Add to close friends</li>
											<li><i className="fas fa-plus"></i> Add to group</li>
											<li><i className="fas fa-ban"></i> Block</li>
										</ul>
									</div>
								</div>
								<div className="card-body msg_card_body" style={{ scrollBehavior: 'smooth' }}>
									{
										chatString.map((item: any) => item)
									}
								</div>
								<div className="card-footer">
									<div className="chatTyping">{typingStr}</div>
									<div className="input-group">
										<div className="input-group-append">
											<span className="input-group-text attach_btn"><i className="fas fa-paperclip"></i></span>
										</div>
										<textarea id='chat-input' value={message} className="form-control type_msg" onChange={(e) => { setMessage(e.target.value); chatTyping(e); }} placeholder="Type your message..."></textarea>
										<div className="input-group-append">
											<span className="input-group-text send_btn" onClick={() => { chat() }}>
												<SendIcon></SendIcon>
											</span>
										</div>
									</div>
								</div>
							</div>
							:
							<div className="card"><div className="card-body msg_card_body" style={{ scrollBehavior: 'smooth' }}></div></div>}
					</div>
				</div>
			</div>
		</>
	);
}