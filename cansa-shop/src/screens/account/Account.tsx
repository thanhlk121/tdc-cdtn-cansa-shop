import React, { useEffect } from 'react'
import HeaderTop from '../../components/header/HeaderTop'
import IconButton from '@material-ui/core/IconButton'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import './account.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import Form from 'react-bootstrap/esm/Form'
import Footer from '../../components/footer/Footer'
import { Link } from 'react-router-dom'
import { getUserInfo } from '../../redux/actions/userActions'
import { logout, State, UserModel, UserStage } from '../../redux'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from "react-router-dom";
import IsLogin from '../../components/IsLogin'

export default function Account() {
    const userState: UserStage = useSelector((state: State) => state.userReducer);
    const { check, userInfor }: { check: boolean, userInfor: UserModel } = userState;
    const history = useHistory();
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getUserInfo());
    }, [check])

    const btnLogout = () => {
        dispatch(logout())
        history.push('/')
    }
    console.log(userInfor)
    return (
        <>
            <IsLogin />
            <HeaderTop></HeaderTop>
            <div className="account_container">
                <div className="container_con_account">
                    <div className="center">
                        <div className="title">
                            <Link className="link_backAccount" to="/">
                                <IconButton className="iconPre">
                                    <ArrowBackIcon></ArrowBackIcon>
                                    <h3>Tài Khoản Của Bạn </h3>
                                </IconButton>
                            </Link>
                        </div>

                        <div className="imageAvatar">
                            <div className="avatarAccount">
                                <div className="image">
                                    <img src={userInfor.user_avatar_image}>
                                    </img>
                                </div>
                            </div>
                            <Form.Label className="name"> <h2>{userInfor.user_real_name}</h2></Form.Label>
                        </div>


                        <div className="btn_account">
                            <button className="btn_infor">
                                <Link className="link_infor" to="/information"> <h3>Thông tin cá nhân</h3></Link>
                            </button>
                            <button className="btn_infor">
                                <Link className="link_infor" to="/ordered"> <h3>Thông tin đơn hàng</h3></Link>
                            </button>
                            <button className="btn_infor">
                                <Link className="link_infor" to="/chat"> <h3>Liên hệ</h3></Link>
                            </button>
                            <button className="btn_out" onClick={() => btnLogout()}><h3>Đăng xuất</h3></button>
                        </div>
                    </div>
                </div>
            </div>
            <Footer></Footer>
        </>
    )

}
