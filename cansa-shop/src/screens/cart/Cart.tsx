import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import CartCard from '../../components/cartcard/CartCard';
import Footer from '../../components/footer/Footer'
import HeaderTop from '../../components/header/HeaderTop'
import { vnd } from '../../consts/Selector';
import cart_empty from '../../images/cart_empty.png'
import { State, CartModel, CartItemModel, updateCart, UserStage } from '../../redux';
import { checkLogin } from '../../redux/actions/userActions';
import './cart.css'

export default function Cart() {
    const dispatch = useDispatch();
    const { cart }: { cart: CartModel } = useSelector((state: State) => state.cartReducer);
    const [_carts, setCarts] = useState<CartItemModel[]>([] as CartItemModel[]);
    const [isLoading, setIsLoading] = useState(false);
    const userState: UserStage = useSelector((state: State) => state.userReducer);
    const { check }: { check: boolean } = userState;
    const history = useHistory();

    useEffect(() => {
        if (cart) {
            setCarts(cart.cart)
            setIsLoading(true);
        }
    }, [cart])

    useEffect(() => {
        dispatch(checkLogin())
    }, [])

    const onTap = (id: number, qty: number) => {
        if(qty === 0 && _carts.length === 1) {
            setIsLoading(true);
        }else{
            setIsLoading(false);
        }
        dispatch(updateCart(id, qty));
    }

    const onCheckout = () =>{
        if (check) {
            history.push("/checkout");
        }else{
            alert('Bạn chưa đang nhập')
            history.push("/Login");
        }
    }
    return (
        <>
            <HeaderTop />
            <div className="cart-container">
                {
                cart ?
                    <div className="cart-container-con">
                        <div className="cart-products-container">
                            {
                                _carts && _carts.map((_cart: CartItemModel) => {
                                    return (
                                        <CartCard isLoading={isLoading} key={_cart.product.product_id} onTap={onTap}cart={_cart} />
                                    )
                                })
                            }
                        </div>
                        <div className="cart-bill">
                            <div className="cart-bill-con">
                                <div className="cart-bill-title">
                                    ĐƠN HÀNG
                                </div>

                                <div className="cart-bill-item">
                                    <div className="cart-bill-name">
                                        Giá tiền
                                    </div>
                                    <div className="cart-bill-price">
                                        {vnd(Number(cart.sub_price))}đ
                                    </div>
                                </div>

                                <div className="cart-bill-item cart-bill-border">
                                    <div className="cart-bill-name">
                                        Phí ship
                                    </div>
                                    <div className="cart-bill-price">
                                        {vnd(Number(cart.ship))}đ
                                    </div>
                                </div>

                                <div className="cart-bill-item">
                                    <div className="cart-bill-total">
                                        Tổng tiền tiền
                                    </div>
                                    <div className="cart-bill-total">
                                        {vnd(Number(cart.total_price))}đ
                                    </div>
                                </div>

                                
                                <div onClick={onCheckout} className="btn-checkout-container">
                                    <button className="btn-checkout">Checkout</button>
                                </div>
                                
                            </div>

                        </div>
                    </div>
                    :
                    <div className="cart-empty">
                        <img src={cart_empty} />
                    </div>
                }

            </div>
            <Footer />
        </>
    )
}
