import React, { useState, useEffect } from 'react';
import Alert from '../../../components/Alert'
import LogoShop from '../../../assets/images/bg-login/256-2560404_leverage-an-open-source-ecommerce-platform-tailored-e.jpg'
import './style4.css'
import HeaderTop from '../../../components/header/HeaderTop';
import { useParams } from 'react-router';
import { useHistory } from "react-router-dom";
import {  ForgottPasswordCenter } from '../../../redux/actions/userActions'
import { State, UserStage } from '../../../redux';
import { useDispatch, useSelector } from 'react-redux';

function ChangePassword() {
    const [message, setMessage] = useState('')
    const history = useHistory();
    const dispatch = useDispatch();
    const userState: UserStage = useSelector((state: State) => state.userReducer);
    const { checkFogotPassword }: { checkFogotPassword:boolean } = userState;
    let { email } = useParams<any>();
    useEffect(()=>{
        if(checkFogotPassword === true){
            history.push(`/Login`);
        }
    },[checkFogotPassword])

    const changepasswordUser = async (e: any) => {
        e.preventDefault()
        const payload = new FormData(e.target);
        if (confirmPassword == password) {
            dispatch(ForgottPasswordCenter(email, password))
        } else {
            alert('Mật khẩu nhập không trùng...')
        }

    }
    const [isError, setIsError] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");


    function checkValuePass(e: any) {
        setConfirmPassword(e.target.value);
        const passwordRegex = new RegExp('^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$')
        if (e.target.value === passwordRegex) {
            setIsError("Mật khẩu gồm 6 ký tự trở lên gồm chữ và số ! ");
        } else {
            setIsError("");
        }
    }

    function checkValuCorfirm(e: any) {
        setPassword(e.target.value);
        if (e.target.value !== confirmPassword) {
            setIsError("Mật khẩu không trùng khớp! Vui lòng nhập lại...");
        } else {
            setIsError("");
        }
    }
    return (
        <>
            <HeaderTop></HeaderTop>
            <section className="passwordretrieval_relative">
                <Alert message={message} />
                <div className="flex justify-center lg:justify-center md:justify-start p-0 md:p-10 overflow-x-hiden">
                    <form id="passwordretrieval_form" onSubmit={(e) => changepasswordUser(e)} className="max-w-sm bg-white rounded-lg shadow-md py-10 px-8">
                        <h1 className="change-container1 text-2xl font-bold w-screen">Lấy Lại Mật Khẩu</h1>
                        <p className="passwordretrieval_p text-gray-400 text-xl mt-5">
                            Thay đổi lại mật khẩu mới của bạn
                        </p>

                        <label className="block text-grey-darker text-sm mb-1  mt-4" >
                            <span className="block mb-1">Mật khẩu</span>
                            <input className="shadow appearance-none border border-gray-400 rounded w-full py-2 px-3 text-grey-darker leading-tight "
                                name="password"
                                type="password"
                                placeholder="Nhập 6 ký tự trở lên"
                                onChange={(e: any) => checkValuePass(e)} />
                        </label>

                        <label className="block text-grey-darker text-sm mb-1  mt-4" >
                            <span className="block mb-1">Nhập lại Mật khẩu</span>
                            <input className="shadow appearance-none border border-gray-400 rounded w-full py-2 px-3 text-grey-darker leading-tight "
                                name="password"
                                type="password"
                                placeholder="Nhập 6 ký tự trở lên"
                                onChange={(e: any) => checkValuCorfirm(e)} />
                        </label>

                        <button type="submit" className="mt-6 btn font-bold w-full">Xác nhận</button>
                        <p className="isError">{isError}</p>
                        <div className="h-px bg-gray-200 mt-8 relative">
                            <span className="absolute absolute-x absolute-y bg-white px-3 mt-px text-sm text-gray-400"></span>
                        </div>
                        <p className="text-gray-400 text-center mt-5">
                            Bạn đã có tài khoản?
                            <a className="text-indigo-700 underline ml-1">Đăng nhập</a>
                        </p>
                    </form>
                    <section className="img_logo hidden md:block">
                        <img className="max-w-lg ml-20 mt-20" src={LogoShop} />
                    </section>
                </div>
            </section>
        </>
    );
}

export default ChangePassword;