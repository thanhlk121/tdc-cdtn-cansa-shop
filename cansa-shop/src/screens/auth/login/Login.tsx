import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Alert from '../../../components/Alert'
import './style.css'
import LogoShop from '../../../assets/images/bg-login/256-2560404_leverage-an-open-source-ecommerce-platform-tailored-e.jpg'
import { useHistory } from "react-router-dom";
import { checkLogin, login, LoginFacebook } from '../../../redux/actions/userActions'
import { State,  UserStage } from '../../../redux';
import { useDispatch, useSelector } from 'react-redux';
// @ts-ignore
import FacebookLogin from 'react-facebook-login';

export default function Login() {
    const [message, setMessage] = useState('')
    const userState: UserStage = useSelector((state: State) => state.userReducer);
    const { check, status }: { check: boolean, status: string } = userState;
    const dispatch = useDispatch();
    const history = useHistory();
    const [email, setEmail] = useState('')
    const [emailValdate, setEmailValdate] = useState(false)
    const [password, setPassword] = useState('')
    const [passwordValdate, setPasswordValdate] = useState(false)

    //Login Facebook result
    const responseFacebook = (response: any) => {
        dispatch(LoginFacebook(response.email, response.accessToken, response.userID, response.name))
    }

    useEffect(() => {
        dispatch(checkLogin());
    }, [status])

    useEffect(() => {
        if (check) {
            dispatch(checkLogin())
            history.push("/");
        }
    }, [check])


    const loginBtn = (e: any) => {
        e.preventDefault()
        if (email !== '' && password !== '') {
            dispatch(login(email, password));
        }

    }

    return (
        <>
            <section className="login_relative">
                <Alert message={message} />
                <div className="flex justify-center lg:justify-center md:justify-start p-0 md:p-10 overflow-x-hidden">
                    <form id="login_form" onSubmit={(e: any) => loginBtn(e)} className="max-w-sm bg-white rounded-lg shadow-md py-10 px-8">
                        <h1 className="login_title text-4xl font-bold w-screen">Đăng Nhập</h1>
                        <p className="login_p text-gray-400 text-xl mt-5">
                            Đăng nhập hoặc tạo tài khoản
                        </p>

                        <label className="block text-grey-darker text-sm mb-1 mt-4" >
                            <span className="block mb-1">Email</span>
                            <input className="shadow appearance-none border border-gray-400 rounded w-full py-2 px-3 text-grey-darker leading-tight "
                                type="email"
                                name="email"
                                placeholder="you@example.com"
                                onChange={(e) => setEmail(e.target.value)} />
                        </label>

                        <label className="block text-grey-darker text-sm mb-1  mt-4" >
                            <span className="block mb-1">Mật khẩu</span>
                            <input className="shadow appearance-none border border-gray-400 rounded w-full py-2 px-3 text-grey-darker leading-tight "
                                name="password"
                                type="password"
                                placeholder="Nhập mật khẩu"
                                onChange={(e) => setPassword(e.target.value)} />
                        </label>
                        <Link to="./EmailOTPscreen" className="forgot_login ">
                            Quên mật khẩu
                        </Link>

                        <button type="submit" className="mt-6 btn font-bold w-full login-linka" onClick={(e: any) => loginBtn(e)}>Đăng nhập</button>
                        {emailValdate && <p>Địa chỉ email của bạn không hợp lệ</p>}
                        {passwordValdate && <p >Mật khẩu của bạn không hợp lệ</p>}
                        <div className="h-px bg-gray-200 mt-8 relative">
                            <span className="absolute absolute-x absolute-y bg-white px-3 mt-px text-sm text-gray-400">HOẶC</span>
                        </div>

                        <Link to="/Register" className="btn login-linka regis-link">Đăng ký</Link>
                        <div className="flex space-x-3 mt-5">
                            <FacebookLogin
                                appId="994248931143640"
                                fields="name,email,picture"
                                scope="public_profile,email"
                                size="small"
                                callback={responseFacebook} />
                        </div>
                        <p className="text-gray-400 text-center mt-5">
                            Bạn đã có tài khoản?
                            <Link className="link-redict" to="/Register"> Đăng ký</Link>
                        </p>
                        <p className="text-gray-400 text-center">
                            Quay lại trang chủ
                            <Link className="link-redict" to="/"> Trang chủ</Link>
                        </p>
                    </form>
                    <section className="img_logo hidden md:block">
                        <img className="max-w-lg ml-20 mt-20" src={LogoShop} />
                    </section>
                </div>
            </section>
        </>
    );
}