import { useEffect, useState } from 'react';
import axios from "axios";
import { Link } from 'react-router-dom';
import Alert from '../../../components/Alert'
import './style1.css'
import LogoShop from '../../../assets/images/bg-login/256-2560404_leverage-an-open-source-ecommerce-platform-tailored-e.jpg'
import { checkLogin, LoginFacebook, register } from '../../../redux/actions/userActions'
import { State, UserStage } from '../../../redux';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from "react-router-dom";
// @ts-ignore
import FacebookLogin from 'react-facebook-login';

function Register() {
    const [message, setMessage] = useState('')
    const userState: UserStage = useSelector((state: State) => state.userReducer);
    const { check, status }: { check: boolean, status: string } = userState;
    const dispatch = useDispatch();
    const history = useHistory();
    const responseFacebook = (response: any) => {
        dispatch(LoginFacebook(response.email, response.accessToken, response.userID, response.name))
    }
    const createUser = async (e: any) => {
        e.preventDefault()
        const payload = new FormData(e.target);
        axios.post('http://happy_eyes.test/api/user/register', payload)
            .then(function (response) {
                setMessage(`Register user success`)
            })
            .catch(function (error) {
                const err = error.response.data.message
                setMessage(err)
            });
    }
    // Regex Form

    const [isError, setIsError] = useState("");
    const [name, setName] = useState('')
    const [nameValdate, setNameValdate] = useState(false)
    const [email, setEmail] = useState('')
    const [emailValdate, setEmailValdate] = useState(false)
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");

    function checkValuePass(e: any) {
        setConfirmPassword(e.target.value);
        if (e.target.value === password) {
            setIsError("Mật khẩu gồm 8 ký tự trở lên gồm chữ và số ! ");
        } else {
            setIsError("");
        }
    }

    function checkValuCorfirm(e: any) {
        setPassword(e.target.value);
        if (e.target.value !== confirmPassword) {
            setIsError("Mật khẩu không trùng khớp! Vui lòng nhập lại...");
        } else {
            setIsError("");
        }
    }

    useEffect(() => {
        dispatch(checkLogin());
        if (!check && status === 'success') {
            history.push("/Login");
        }
    }, [status])

    useEffect(() => {
        if (check) {
            dispatch(checkLogin())
            history.push("/");
        }
    }, [check])

    const registerBtn = (e: any) => {
        e.preventDefault()
        dispatch(register(email, password, name));
    }
    
    return (
        <>
            <section className="register_relative">
                <Alert message={message} />
                <div className="flex justify-center lg:justify-center md:justify-start p-0 md:p-10 overflow-x-hidden">
                    <form id="register_form" onSubmit={(e) => createUser(e)} className="max-w-sm bg-white rounded-lg shadow-md py-10 px-8">
                        <h1 className="register-screen text-2xl font-bold w-screen">Đăng Ký</h1>
                        <p className="register_p text-gray-400 text-xl mt-5">
                            Đăng ký tài khoản của bạn
                        </p>
                        <label className="block text-grey-darker text-sm mb-1 mt-4" >
                            <span className="block mb-1">Nick Name</span>
                            <input className="shadow appearance-none border border-gray-400 rounded w-full py-2 px-3 text-grey-darker leading-tight "
                                type="text"
                                name="name"
                                placeholder="Ex: hoanganh34k"
                                onChange={(e) => setName(e.target.value)} />
                        </label>

                        <label className="block text-grey-darker text-sm mb-1 mt-4" >
                            <span className="block mb-1">Email</span>
                            <input className="shadow appearance-none border border-gray-400 rounded w-full py-2 px-3 text-grey-darker leading-tight "
                                type="email"
                                name="email"
                                placeholder="you@example.com"
                                onChange={(e) => setEmail(e.target.value)} />
                        </label>

                        <label className="block text-grey-darker text-sm mb-1  mt-4" >
                            <span className="block mb-1">Mật khẩu</span>
                            <input className="shadow appearance-none border border-gray-400 rounded w-full py-2 px-3 text-grey-darker leading-tight "
                                name="password"
                                type="password"
                                placeholder="Nhập 6 ký tự trở lên"
                                onChange={(e: any) => checkValuePass(e)} />
                        </label>

                        <label className="block text-grey-darker text-sm mb-1  mt-4" >
                            <span className="block mb-1">Nhập lại Mật khẩu</span>
                            <input className="shadow appearance-none border border-gray-400 rounded w-full py-2 px-3 text-grey-darker leading-tight "
                                name="confirmpassword"
                                type="password"
                                placeholder="Nhập 6 ký tự trở lên"
                                onChange={(e: any) => checkValuCorfirm(e)} />
                        </label>

                        <button type="submit" onClick={(e: any) => registerBtn(e)} className="mt-6 btn font-bold w-full login-linka">Đăng ký</button>
                        <div>
                            {nameValdate && <p className="isError">Tên của bạn không hợp lệ</p>}
                            {emailValdate && <p className="isError">Địa chỉ email của bạn không hợp lệ</p>}
                            <p className="isError">{isError}</p>
                        </div>

                        <div className="h-px bg-gray-200 mt-8 relative">
                            <span className="absolute absolute-x absolute-y bg-white px-3 mt-px text-sm text-gray-400">HOẶC</span>
                        </div>

                        <Link to="/Login" className="btn block mt-6 w-full login-linka">Đăng nhập</Link>
                        <div className="flex space-x-3 mt-5">
                            <FacebookLogin
                                appId="994248931143640"
                                fields="name,email,picture"
                                scope="public_profile,email"
                                size="small"
                                callback={responseFacebook} />
                        </div>
                        <p className="text-gray-400 text-center mt-5">
                            Bạn đã có tài khoản?
                            <Link className="link-redict" to="/Login"> Đăng nhập</Link>
                        </p>
                        <p className="text-gray-400 text-center">
                            Quay lại trang chủ
                            <Link className="link-redict" to="/"> Trang chủ</Link>
                        </p>
                    </form>
                    <section className="img_logo hidden md:block">
                        <img className="max-w-lg ml-20 mt-20" src={LogoShop} />
                    </section>
                </div>
            </section>
        </>
    );
}

export default Register;
