import Footer from '../../components/footer/Footer'
import HeaderTop from '../../components/header/HeaderTop'
import './productdetail.css'
import Gallery from './../../components/gallery/Gallery';
import ReactStars from 'react-stars';
import Rating from '../../components/Rating/Rating';
import Comment from '../../components/comment/Comment';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import { getProduct, State, ProductModel, CommentModel, UserStage, getCart } from '../../redux';
import { addComment, getComments } from '../../redux/actions/commentActions';
import { getUserInfo } from '../../redux/actions/userActions';
import Loading from '../../components/loading/Loading';
import { vnd } from '../../consts/Selector';
import { addCart } from '../../redux/actions/cartActions';
import { Link } from 'react-router-dom';



export default function ProductDetail() {
    let { id } = useParams<any>();
    const [isLoading, setIsLoading] = useState(false);
    const [page, setPage] = useState<number>(1);
    const dispatch = useDispatch();
    const productState = useSelector((state: State) => state.productReducer);
    const commentState = useSelector((state: State) => state.commentReducer);
    const userState: UserStage = useSelector((state: State) => state.userReducer);
    const cartState = useSelector((state: State) => state.cartReducer);
    const { product }: { product: ProductModel } = productState;
    const { comment }: { comment: CommentModel[] } = commentState;
    const { userInfor } = userState;
    const { status }: { status: string } = cartState;
    const [isLoadingCart, setIsLoadingCart] = useState(true);
    const [isLoadMore, setisLoadMore] = useState(false);

    useEffect(() => {
        setTimeout(() => {
            setIsLoading(true);
        }, 500);
    }, [product])

    useEffect(() => {
        setisLoadMore(false)
    }, [commentState])

    useEffect(() => {
        if (status && !isLoadingCart) {
            const message = (status === 'success') ? 'Đã thêm vào giỏ hàng' : 'Thêm vào giỏ hàng thất bại';
            alert(message);
            setIsLoadingCart(true)
        }
        dispatch(getCart());
    }, [cartState])

    useEffect(() => {
        setisLoadMore(true)
        dispatch(getComments(id, page));
    }, [page])

    useEffect(() => {
        window.scrollTo(0, 0)
        setIsLoading(false);
        dispatch(getProduct(id));
        dispatch(getComments(id));
        dispatch(getUserInfo());
    }, []);

    const onTap = (comment_content: string, comment_rating: number) => {
        if (userInfor) {
            if (comment_content) {
                dispatch(addComment(id, userInfor.user_id, comment_content, comment_rating));
            }
        } else {
            alert("Chưa đăng nhập");
        }
    }

    const addToCart = () => {
        if (isLoadingCart) {
            setIsLoadingCart(false);
            dispatch(addCart(id));
        }
    }
    return (
        <>
            <HeaderTop />
            {
                (product && isLoading) ?
                    <div className="productDetail-conainer">
                        <div className="productDetail-conainer-con">
                            <div className="productDetail-left-bar">
                                <Gallery avatar={product.product_avatar} images={product.product_image} />
                            </div>
                            <div className="productDetail-right-bar">
                                <div className="productDetail-right-title">
                                    {product.product_title}
                                </div>
                                {   product.product_rating?
                                    <div className="productDetail-rating-medium">
                                        <ReactStars
                                            half={true}
                                            edit={false}
                                            value={Number.parseFloat(product.product_rating.toFixed(1))}
                                            count={5}
                                            size={24}
                                            color2={'#ffd700'}
                                        />
                                        <div className="productDetail-rating-txt">
                                            {product.product_rating.toFixed(1)}
                                        </div>
                                    </div>
                                    :
                                    <div className="product-rating-none">
                                        Chưa đánh giá
                                    </div>
                                    
                                }
                                <div className="productDetail-center">
                                    <div className="productDetail-price-container">
                                        {product && product.product_sale! !== 0 &&
                                            <div className="productDetail-price-sale">
                                                {vnd(product.product_price!)}đ
                                            </div>
                                        }

                                        <div className="productDetail-price">
                                            {product && vnd(product.product_price * (100 - product.product_sale) / 100)}đ
                                        </div>
                                    </div>
                                    <div className="productDetail-btn">
                                        {
                                            isLoadingCart ?
                                            <button onClick={addToCart} className="productDetail-btn-add">Thêm</button>
                                            :
                                            <button className="productDetail-btn-add">Thêm</button>
                                        }
                                        <button className="productDetail-btn-check"><Link className="productdetail-link-shop" to={`/shopdetail/${product.shop_id}`}>Shop</Link></button>

                                    </div>
                                </div>
                                <div className="productDetail-desc">
                                    <div className="productDetail-desc-title">
                                        Mô tả :
                                    </div>
                                    <div className="productDetail-desc-spn">
                                        {product.product_description}
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div className="comment">
                            <div className="comment-desc">
                                <div className="productDetail-desc-title">
                                    Đánh giá và nhận xét :
                                </div>
                            </div>
                            <Rating onTap={onTap} />
                            {
                                comment &&
                                comment.map((item: CommentModel, index: number) => <Comment key={item.comment_id} comment={item} />)
                            }
                        </div>
                        {
                            isLoadMore ?
                            <div className="categories-load-more">
                                <Loading type="max" />   
                            </div>
                            :   
                            <div onClick={()=>setPage(page+1)} className="categories-load-new">
                                Xem thêm . . .  
                            </div>
                        }
                    </div>
                    :
                    <div className="productDetail-load">
                        <Loading type="screen" />
                    </div>
            }
            <Footer />
        </>

    )
}

