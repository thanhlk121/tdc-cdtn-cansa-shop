import Home from './screens/home/Home';
import './assets/css/dist/tailwind.css';
import ProductDetail from './screens/productdetail/ProductDetail';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Information from './screens/information/Information';
import UpdateInformation from './screens/updateInformation/UpdateInformation';
import Account from './screens/account/Account';
import CheckOut from './screens/checkout/CheckOut';
import Categories from './screens/categories/Categories';
import Cart from './screens/cart/Cart';
import SearchScreen from './screens/search/SearchScreen';
import Ordered from './screens/ordered/Ordered';
import OrderDetail from './screens/orderdetail/OrderDetail';
import Register from './screens/auth/Register/Register';
import PasswordRetrieval from './screens/auth/ForgotPassword/PasswordRetrieval';
import EmailOTPscreen from './screens/auth/EmailOTP/EmailOTPscreen';
import OTP from './screens/auth/OTPscreen/OTP';
import ChangePassword from './screens/auth/ChangePassword/ChangePassword';
import Login from './screens/auth/login/Login';
import Chat from './screens/chat/Chat';
import ShopDetail from './screens/shopdetail/ShopDetail';

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/Login" component={Login} />
          <Route exact path="/product-detail/:product_title/:id" component={ProductDetail} />
          <Route exact path="/Register" component={Register} />
          <Route exact path="/PasswordRetrieval/:email/:codepin" component={PasswordRetrieval} />
          <Route exact path="/EmailOTPscreen" component={EmailOTPscreen} />
          <Route exact path="/OTP/:email" component={OTP} />
          <Route exact path="/ChangePassword" component={ChangePassword} />
          <Route exact path="/information" component={Information} />
          <Route exact path="/updateinformation" component={UpdateInformation} />
          <Route exact path="/account" component={Account} />
          <Route exact path="/checkout" component={CheckOut} />
          <Route exact path="/categories" component={Categories} />
          <Route exact path="/cart" component={Cart} />
          <Route exact path="/products/:title/:data" component={SearchScreen} />
          <Route exact path="/ordered" component={Ordered} />
          <Route exact path="/orderdetail/:oder_id" component={OrderDetail} />
          <Route exact path="/shopdetail/:shop_id" component={ShopDetail} />
          <Route exact path="/chat" component={Chat} />
          <Route exact path="/chat/:id" component={Chat} />
        </Switch>
      </Router>

    </div>
  );
}

export default App;
